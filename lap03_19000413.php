<html>

<head>
  <meta charset="UTF-8">
</head>

<body>
  <style>
    <?php
    $mangTQ = array(
      "gioi tinh" => array(0 => "Nam", 1 => "Nữ"),
      "Khoa" => array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu")
    );
    ?>body {
      color: BLACK;
      margin-top: 50px;
      margin-left: auto;
      margin-right: auto;
      width: 500px;
      height: 500px;
      border: 3px solid #85b5dd;
      padding: auto;
    }

    .timeCurrent {
      margin-top: 10px;
      margin-left: 15%;
      margin-right: 15%;
      background: #f2f2f2;
      height: 30px;
      text-align: left;
      padding-left: 5px;
      padding-top: 10px;
    }

    .login {
      margin-left: 15%;
      background: #5b9bd5;
      color: White;
      height: 40px;
      line-height: 40px;
      text-align: center;
      width: 150px;
      margin-right: 5px;
    }

    .gender {
      margin-left: 15%;
      margin-right: 5px;
      background: #5b9bd5;
      color: White;
      height: 40px;
      line-height: 40px;
      text-align: center;
      width: 150px;
    }

    .faculty {
      margin-left: 15%;
      margin-right: 5px;
      margin-top: 30px;
      background: #5b9bd5;
      color: White;
      height: 40px;
      line-height: 40px;
      text-align: center;
      width: 150px;
    }

    .textfeild1 {
      margin-left: 15px;
      margin-bottom: 20px;
      padding-top: 10px;
      padding-right: 10px;
      padding-bottom: 10px;
      padding-left: 20px;
      border: 2px solid #7598B7;

    }

    .textfeild2 {
      margin-left: 15px;
      margin-top: 34px;
      width: 200px;
      padding-top: 15px;
      border: 2px solid #7598B7;
      background: white;
    }

    .line1 {
      margin-top: 50px;
      display: flex;
    }

    .line2 {
      margin-top: 20px;
      display: flex;
    }

    .line3 {
      margin-top: 20px;
      display: flex;
    }

    .buttonInput {
      margin-left: 35%;
      width: 150px;
      height: 40px;
      background: #70AD47;
      border: 3px solid #527da4;
      box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
      color: White;
      border-radius: 10px;
      align-content: center;
    }

    label {
      padding-top: 10px;
    }

  

    .rounded-checkbox {
      /* padding-left: 10px;
  padding-right: 10px;*/
      margin-top: 10px;
      margin-left: 20px;
      width: 15px;
      height: 15px;
      border-radius: 50%;
      vertical-align: middle;
      border: 2px solid #42719C;
      appearance: none;
      -webkit-appearance: none;
      outline: none;
      cursor: pointer;
      background: #5B9BD5
    }

    .rounded-checkbox:checked {
      clip-path: circle(50% at 50% 50%);
      /* padding-left: 10px;
  padding-right: 10px; */
      margin-top: 10px;
      width: 15px;
      height: 15px;
      border-radius: 50%;
      vertical-align: middle;
      border: 2px solid #42719C;
      appearance: none;
      -webkit-appearance: none;
      outline: none;
      cursor: pointer;
      background-color: green;
    }

    .triangle {
      width: 0;
      height: 0;
      border: solid 30px black;
      border-color: #5B9BD5 transparent transparent transparent;
      position: relative;
      left: 150px;
      bottom: 33px;
    }

    .formLogin {
      margin-left: auto;
      margin-right: auto;
    }

    .dropdown .dropbtn {
      font-size: 16px;
      border: none;
      outline: none;
      background-color: inherit;
      font-family: inherit;
    }

    .dropdown-content {
      display: none;
      position: absolute;
      bottom: 44%;
      left: 50%;
      background-color: #f9f9f9;
      min-width: 160px;
      box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
      z-index: 1;
    }

    .dropdown {
      float: left;
      overflow: hidden;
    }

    .dropdown-content a {
      float: none;
      color: black;
      padding: 12px 16px;
      text-decoration: none;
      display: block;
      text-align: left;
    }

    .dropdown-content a:hover {
      background-color: #ddd;
    }

    .dropdown:hover .dropdown-content {
      display: block;
    }

    .gendlb {
      margin-top: 10px;
    }
  </style>

  <form method="post">
    <div class="line1">
      <div class="login">Họ và tên</div>
      <input class="textfeild1" type="text" name="user" />
    </div>
    <div class="line2">
      <div class="gender"> Giới tính </div>
      <?php
      for ($i = 0; $i <= 1; $i++) {   ?>
        <input type="checkbox" class="rounded-checkbox" id="checkbox" />
        <?php echo "<div class=\"gendlb\">{$mangTQ["gioi tinh"][$i]}</div>"; ?>
        <!-- <label class="gendlb" for="checkbox">Nam</label> -->
        <!-- '{$mangTQ["gioi tinh"][$i]}' -->
      <?php } ?>


    </div>
    </div>
    <div class="line3">
      <div class="faculty"> Phân khoa </div>
      <div class="dropdown">
        <div class="dropdownmenu">
            <input class="textfeild2" type="button" name="facultytext" />
            <div class="triangle">
            </div>
        </div>
        <div class="dropdown-content">
          <!-- <a href="#">  </a>
        <a href="#"> Khoa học máy tính </a>
        <a href="#"> Khoa học vật liệu </a> -->
          <!-- <select name="dropdownseclect" id="select"> -->
          <?php foreach ($mangTQ["Khoa"] as $faculity) : ?>
            <a href="#">
              <option value="fact"> <?php echo $faculity ?> </option>
            </a>
          <?php endforeach; ?>
          <!-- </select> -->
        </div>


      </div>
    </div>
    <div class="buttonInputdiv">
      <input class="buttonInput" type="submit" name="register" value="Đăng ký" id="submitBtn" />


      </script>
  </form>
</body>

</html>